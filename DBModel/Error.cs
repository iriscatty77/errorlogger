namespace DBModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Error
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Error()
        {
            Application = new Application();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ErrorID { get; set; }

        [Column(TypeName = "text")]
        public string ErrorDescription { get; set; }

        public int ErrorType { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ErrorDateCreated { get; set; }

        public int AppID { get; set; }

        public Application Application { get; set; }
    }
}
