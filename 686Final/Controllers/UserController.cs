﻿using _686Final.Base;
using Common;
using LoadersAndLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace _686Final.Controllers
{
    public class UserController : BaseController
    {

        UserLoader userLoader = new UserLoader();

        // GET: User
        public ActionResult Index()
        {
            return View(userLoader.GetAllUsers());
        }

        // GET: User/User/5
        public ActionResult CurrentUser(string email)
        {
            String user = System.Web.HttpContext.Current.User.Identity.Name;
            if (user == email)
            {
                return View(userLoader.GetUserByEmail(email));
            }
            else {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            UserViewModel user = userLoader.GetUserByIdForAdmin(id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else {
                return View(user);
            }
            
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            UserViewModel user = userLoader.GetUserByIdForAdmin(id);

            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            List<SelectListItem> roleDescs = new List<SelectListItem>();
            userLoader.getAllRoles().ToList().ForEach(delegate (RoleViewModel item) {
                SelectListItem sItem = new SelectListItem();
                sItem.Text = item.RoleDescript;
                Debug.WriteLine(item.RoleDescript);
                sItem.Value = item.RoleID.ToString();
                if (item.RoleID == user.RoleID) {
                    sItem.Selected = true;
                    Debug.WriteLine("Default is: " + user.RoleID);
                }
                roleDescs.Add(sItem);
            });

            List<SelectListItem> stateDescs = new List<SelectListItem>();
            userLoader.getAllStates().ToList().ForEach(delegate (StateViewModel item) {
                SelectListItem sItem = new SelectListItem();
                sItem.Text = item.StateDescript;
                sItem.Value = item.StateID.ToString();
                if (item.StateID == user.StateID)
                {
                    sItem.Selected = true;
                    Debug.WriteLine("Default is: " + user.StateID);
                }
                stateDescs.Add(sItem);
            });

            ViewBag.roleDescs = roleDescs;
            ViewBag.stateDescs = stateDescs;
            return View(user);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, [Bind(Include = "RoleViewModel, StateViewModel")] UserViewModel user, int[] userNewAppIDs)
        {
            
            try
            {
                userLoader.updateUser(id, user, userNewAppIDs);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



    }
}
