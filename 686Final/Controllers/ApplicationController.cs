﻿using _686Final.Base;
using _686Final.Helper;
using Common;
using LoadersAndLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace _686Final.Controllers
{
    public class ApplicationController : BaseController
    {

        AppLoader appLoader = new AppLoader();

        // GET: Application
        public ActionResult Index()
        {
            // log a error each time launch the application home page.
            // FinalLogger.finalLogger.Log("Error from Application Comtroller", 1, null);
            return View(appLoader.getAllApplications());
        }

        // GET: Application/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationViewModel application = appLoader.getApplication(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }


        // GET: Application/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationViewModel application = appLoader.getApplication(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Application/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, [Bind(Include = "AppDescription,AppType, AppActive")] ApplicationViewModel application)
        {
            if (ModelState.IsValid)
            {
                appLoader.updateApp(id, application);
                return RedirectToAction("Index");
            }
            return View(application);
        }


        // GET: App/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: App/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AppDescription,AppType")] ApplicationViewModel applicationViewModel)
        {
            if (ModelState.IsValid)
            {
                appLoader.createNewApp(applicationViewModel);
                return RedirectToAction("Index");
            }

            return View(applicationViewModel);
        }



    }
}
