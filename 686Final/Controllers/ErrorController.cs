﻿using _686Final.Base;
using Common;
using LoadersAndLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace _686Final.Controllers
{
    public class ErrorController : BaseController
    {
        ErrorLoader errorLoader = new ErrorLoader();

        // GET: Error
        public ActionResult Index()
        {
            ICollection<ErrorViewModel> data = errorLoader.GetAllErrors();
            Debug.WriteLine("========================: " + data.Count);
            return View(data);
            
        }

        // GET: Error/AppErrors/5
        public ActionResult AppErrors(int appID)
        {
            String user = System.Web.HttpContext.Current.User.Identity.Name;
            if (errorLoader.isUserAthorized(user, appID))
            {
                return View(errorLoader.GetErrorsByAppID(appID));
            }
            else {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            
        }

    }
}
