﻿
using _686Final.Base;
using Common;
using LoadersAndLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _686Final.Controllers
{
    public class HomeController : BaseController
    {

        public ActionResult Index()
        {
            
            String user = System.Web.HttpContext.Current.User.Identity.Name;
            RoleEnum currentUserRole = (RoleEnum) Session["CurrentUserRole"];

            switch (currentUserRole)
            {
                case RoleEnum.Admin:
                    return RedirectToAction("Index", "User");
                case RoleEnum.User:
                    return RedirectToAction("CurrentUser", "User", new { email =  user});
                default:
                    return View();
            }     
        }

    

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Error()
        {
            return View();
        }



    }
}