﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_686Final.Startup))]
namespace _686Final
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
