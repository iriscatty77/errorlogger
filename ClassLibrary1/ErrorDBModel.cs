namespace ClassLibrary1
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ErrorDBModel : DbContext
    {
        public ErrorDBModel()
            : base("name=ErrorDBModel")
        {
        }

        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<Error> Errors { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Application>()
                .Property(e => e.AppDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Application>()
                .HasMany(e => e.Errors)
                .WithMany(e => e.Applications)
                .Map(m => m.ToTable("AppError").MapLeftKey("AppID").MapRightKey("ErrorID"));

            modelBuilder.Entity<Error>()
                .Property(e => e.ErrorDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.RoleDescript)
                .IsUnicode(false);

            modelBuilder.Entity<State>()
                .Property(e => e.StateDescript)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserPassword)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Applications)
                .WithMany(e => e.Users)
                .Map(m => m.ToTable("UserApp").MapLeftKey("UserID").MapRightKey("AppID"));
        }
    }
}
