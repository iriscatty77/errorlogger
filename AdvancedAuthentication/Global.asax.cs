﻿/*
    Dusan Palider
    CSE 686
    Spring 2016

    Demonstrates advanced authentication approach (one of many, of course) and how we can ensure that every controller action has to 
    be authenticated.
*/

namespace AdvancedAuthentication
{
    using System.Web.Mvc;
    using System.Web.Routing;
    using Security;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // Initialize the security matrix
            SecurityMatrix.Initialize();
        }
    }
}
