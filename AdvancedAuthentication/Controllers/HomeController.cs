﻿/*
    Dusan Palider
    CSE 686
    Spring 2016

    Please consult global.asax for information about this code
*/

namespace AdvancedAuthentication.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Home controller
    /// </summary>
    public class HomeController : BaseController
    {
        /// <summary>
        /// Home page
        /// </summary>
        public ActionResult Index()
        {
            Session["userId"] = "hello world";
            return View();
        }

        /// <summary>
        /// Requires User role
        /// </summary>
        public ActionResult UserPage()
        {
            return View();
        }

        /// <summary>
        /// Requires Admin role
        /// </summary>
        /// <returns></returns>
        public ActionResult AdminPage()
        {
            return View();
        }

        /// <summary>
        /// Error page, displayed when an error occured
        /// </summary>
        public ActionResult Error()
        {
            return View();
        }
    }
}