﻿using Common;
using LoadersAndLogic;
using Logger.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Logger.Controllers
{
    public class ErrorController : ApiController
    {
        private ErrorLoader errorloader  = new ErrorLoader();

        private BufferQueue bq = new BufferQueue();

        [HttpGet]
        public ICollection<ErrorViewModel> GetError()
        {

            return errorloader.GetAllErrors();
        }

        [HttpPost]
        public HttpResponseMessage SaveError(ErrorViewModel error)
        {
            try {
                errorloader.SaveError(error);
                return Request.CreateResponse(HttpStatusCode.OK);
            } catch (Exception e) {
                Debug.WriteLine("Error added to Queue in Rest API: " + e);
                BufferQueue.bufferQueue.Enqueue(error);
                BufferQueue.processQueuedItem(); 
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Request Failed"); 
            }
            
        }
    }
}
