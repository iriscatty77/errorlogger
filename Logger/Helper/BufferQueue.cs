﻿using Common;
using LoadersAndLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;

namespace Logger.Helper
{
    public class BufferQueue
    {
        private static ErrorLoader errorLoader = new ErrorLoader();

        public static Queue<ErrorViewModel> bufferQueue;

        public static void processQueuedItem()
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (true)
                {
                    if (bufferQueue.Count == 0)
                    {
                        Thread.Sleep(100000);
                    }
                    else
                    {
                        ErrorViewModel error = bufferQueue.Dequeue();
                        try {
                            errorLoader.SaveError(error);
                        } catch (Exception e) {
                            Debug.WriteLine(e);
                            bufferQueue.Enqueue(error);
                            // sleep longer due to keep failing
                            Thread.Sleep(2000000);
                        }    
                    }
                }
            });
        }
    }
}