﻿

namespace LoadersAndLogic
{
    using DBModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Common;
    using System.Diagnostics;
    using System.Data.Entity;

    public class UserLoader
    {
       
        ErrorDBModel db = new ErrorDBModel();
  
 
        public ICollection<UserViewModel> GetAllUsers()
        {

            ICollection<UserViewModel> users= new List<UserViewModel> ();
            foreach (User dbUser in db.Users.ToList()){
                UserViewModel User = new UserViewModel() {
                    UserID = dbUser.UserID,
                    EmailAddress = dbUser.EmailAddress,
                    FirstName = dbUser.FirstName,
                    LastName = dbUser.LastName,
                    LastLoginDate = dbUser.LastLoginDate,
                    RoleID = dbUser.RoleID,
                    StateID = dbUser.StateID
                 };
                users.Add(User);
            }

            return users;
            
        }

        public UserViewModel GetUserByEmail(string email)
        {
            User dbUser = db.Users
                .Where(u => u.EmailAddress == email)
                .Include(u => u.Role)
                .Include(u => u.State)
                .Include(u => u.Applications)
                .Single();

            UserViewModel User = new UserViewModel()
            {
                UserID = dbUser.UserID,
                EmailAddress = dbUser.EmailAddress,
                FirstName = dbUser.FirstName,
                LastName = dbUser.LastName,
                LastLoginDate = dbUser.LastLoginDate,
                RoleID = dbUser.RoleID,
                RoleViewModel = new RoleViewModel()
                {
                    RoleID = dbUser.Role.RoleID,
                    RoleDescript = dbUser.Role.RoleDescript
                },
                StateID = dbUser.StateID,
                StateViewModel = new StateViewModel()
                {
                    StateID = dbUser.State.StateID,
                    StateDescript = dbUser.State.StateDescript
                },
                ApplicationViewModel = new List<ApplicationViewModel>()
                
            };

            if (dbUser.StateID != 0) {
                ICollection<Application> dbUserApps = dbUser.Applications.Where(a => a.AppActive == true).ToList();
                Debug.WriteLine("============ user App's count: " + dbUserApps.Count);
                ICollection<ApplicationViewModel> userApps = new List<ApplicationViewModel>();
                foreach (var dbUserApp in dbUserApps)
                {
                    ApplicationViewModel userApp = new ApplicationViewModel()
                    {
                        AppID = dbUserApp.AppID,
                        AppDateCreated = dbUserApp.AppDateCreated,
                        AppDescription = dbUserApp.AppDescription,
                        AppType = dbUserApp.AppType
                    };
                    userApps.Add(userApp);
                }
                User.ApplicationViewModel = userApps;
            }
       
            return User;


        }

        public static DateTime getAndSaveLastLoginDate(string email)
        {
            ErrorDBModel db = new ErrorDBModel();
            User dbUser = db.Users
                .Where(u => u.EmailAddress == email).Single();
            DateTime? lastLoginDate = dbUser.LastLoginDate;
            dbUser.LastLoginDate = DateTime.UtcNow.ToLocalTime();
            db.SaveChanges();
            return (lastLoginDate ?? DateTime.UtcNow.ToLocalTime()); 
            
        }

        public UserViewModel GetUserById(int id)
        {
            User dbUser = db.Users
                                .Include(u => u.Applications)
                                .Where(i => i.UserID == id)
                                .Single();

            ICollection<Application> dbUserApps = dbUser.Applications;
            Debug.WriteLine("============ user App's count: " + dbUserApps.Count);
            ICollection<ApplicationViewModel> userApps = new List<ApplicationViewModel>();
            foreach (var dbUserApp in dbUserApps) {
                ApplicationViewModel userApp = new ApplicationViewModel()
                {
                    AppID = dbUserApp.AppID,
                    AppDateCreated = dbUserApp.AppDateCreated,
                    AppDescription = dbUserApp.AppDescription,
                    AppType = dbUserApp.AppType
                };
                userApps.Add(userApp);
            }

            UserViewModel User = new UserViewModel()
            {
                UserID = dbUser.UserID,
                EmailAddress = dbUser.EmailAddress,
                FirstName = dbUser.FirstName,
                LastName = dbUser.LastName,
                LastLoginDate = dbUser.LastLoginDate,
                RoleID = dbUser.RoleID,
                StateID = dbUser.StateID,
                ApplicationViewModel = userApps
            };
 
            return User;
        }

        public UserViewModel GetUserByIdForAdmin(int id)
        {
            User dbUser = db.Users
                                .Include(u => u.Applications)
                                .Include(u => u.Role)
                                .Include(u => u.State)
                                .Where(i => i.UserID == id)
                                .Single();

            if (dbUser == null) {
                return null;
            }

            ICollection<int> userAppIDs = new HashSet<int>(dbUser.Applications.Select(i => i.AppID));
            ICollection<Application> allDBApps = db.Applications.Where(a => a.AppActive == true).ToList();
            ICollection<ApplicationViewModel> allApps = new List<ApplicationViewModel>();
            foreach (var dbApp in allDBApps) {
                ApplicationViewModel userApp = new ApplicationViewModel()
                {
                    AppID = dbApp.AppID,
                    AppDateCreated = dbApp.AppDateCreated,
                    AppDescription = dbApp.AppDescription,
                    AppType = dbApp.AppType,
                    Active = userAppIDs.Contains(dbApp.AppID)
                };
                allApps.Add(userApp);

            }

            UserViewModel User = new UserViewModel()
            {
                UserID = dbUser.UserID,
                EmailAddress = dbUser.EmailAddress,
                FirstName = dbUser.FirstName,
                LastName = dbUser.LastName,
                LastLoginDate = dbUser.LastLoginDate,
                RoleID = dbUser.RoleID,
                RoleViewModel = new RoleViewModel() {
                    RoleID = dbUser.Role.RoleID,
                    RoleDescript = dbUser.Role.RoleDescript
                },
                StateID = dbUser.StateID,
                StateViewModel = new StateViewModel() {
                    StateID = dbUser.State.StateID,
                    StateDescript = dbUser.State.StateDescript
                },
                ApplicationViewModel = allApps
            };

            return User;
        }

        public void Register(UserViewModel user)
        {
            User dbUser = new User()
            {
                EmailAddress = user.EmailAddress,
                FirstName = user.FirstName,
                LastName = user.LastName,
                LastLoginDate = user.LastLoginDate,
                RoleID = user.RoleID,
                StateID = user.StateID
            };
            db.Users.Add(dbUser);
            db.SaveChanges();
        }

        public void updateUser(int userID, UserViewModel user, int[] userNewAppIDs) {

            User dbUser = db.Users
                            .Include(i => i.Applications)
                            .Include(i => i.Role)
                            .Include(i => i.State)
                            .Where(i => i.UserID == userID)
                            .Single();
            ICollection<Application> oldApps = dbUser.Applications;

            ICollection<int> oldAppIDs = new HashSet<int>(dbUser.Applications.Select(i => i.AppID));


            if (userNewAppIDs != null)
            {

                ICollection<Application> newApps = db.Applications.Where(i => userNewAppIDs.Contains(i.AppID)).ToList();
                

                foreach (var oldAppID in oldAppIDs)
                {
                    if (!userNewAppIDs.Contains(oldAppID))
                    {
                        oldApps.Remove(db.Applications.Find(oldAppID));
                    }
                }

                foreach (var newApp in newApps)
                {
                    if (!oldApps.Contains(newApp))
                    {
                        dbUser.Applications.Add(newApp);
                    }
                }

            }
            else {
                foreach (var oldAppID in oldAppIDs)
                {
                    oldApps.Remove(db.Applications.Find(oldAppID));  
                }
            }
            dbUser.RoleID = user.RoleViewModel.RoleID;
            dbUser.Role = db.Roles.Find(user.RoleViewModel.RoleID);
            dbUser.StateID = user.StateViewModel.StateID;
            dbUser.State = db.States.Find(user.StateViewModel.StateID);

            Debug.WriteLine(user.RoleViewModel.RoleDescript);
            Debug.WriteLine(user.StateViewModel.StateDescript);
            db.SaveChanges();
        }

        public ICollection<RoleViewModel> getAllRoles() {
            ICollection<RoleViewModel> result = new List<RoleViewModel>();
            db.Roles.ToList().ForEach(delegate (Role item)
            {
                result.Add(new RoleViewModel()
                {
                    RoleID = item.RoleID,
                    RoleDescript = item.RoleDescript
                });
            });
            return result;
        }

        public ICollection<StateViewModel> getAllStates() {
            ICollection<StateViewModel> result = new List<StateViewModel>();
            db.States.ToList().ForEach(delegate (State item)
            {
                result.Add(new StateViewModel()
                {
                    StateID = item.StateID,
                    StateDescript = item.StateDescript
                });
            });
            return result;
        }


        public bool isUserAthorized(string user, int appID)
        {
            User dbUser = db.Users.Include(u => u.Applications).ToList()
                                .Where(u => u.EmailAddress == user)
                                .Single();
            ICollection<int> appIDs = new HashSet<int>(dbUser.Applications.Select(i => i.AppID));
            return (appIDs.Contains(appID));

        }
    }
}
