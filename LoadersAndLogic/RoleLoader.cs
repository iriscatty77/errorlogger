﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DBModel;

namespace LoadersAndLogic
{
    public class RoleLoader
    {
        ErrorDBModel db = new ErrorDBModel();

        public RoleEnum getRoleByUserName(String userName) {
            if (userName == null || userName.Count()==0) {
                return RoleEnum.None;
            }
            int? roleID = db.Users
                            .Where(u => u.EmailAddress == userName)
                            .Single()
                            .RoleID;

            int role = roleID ?? default(int);
            return getRoleEnam(role);
        }

        private RoleEnum getRoleEnam(int role)
        {
            RoleEnum roleEnum; 
            switch (role) {
                case 3:
                    roleEnum = RoleEnum.User;
                    break;
                case 8:
                    roleEnum = RoleEnum.Admin;
                    break;
                default:
                    roleEnum = RoleEnum.None;
                    break;
            }
            return roleEnum;

        }
    }
}
