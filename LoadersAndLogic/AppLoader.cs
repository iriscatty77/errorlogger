﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBModel;
using Common;
using System.Diagnostics;
using System.Data.Entity;

namespace LoadersAndLogic
{
    public class AppLoader
    {
        ErrorDBModel db = new ErrorDBModel();

        public ApplicationViewModel getApplication(int appID) {
           
            Application app =  db.Applications.Find(appID);
            return new ApplicationViewModel()
            {
                AppID = app.AppID,
                AppDateCreated = app.AppDateCreated,
                AppDescription = app.AppDescription,
                AppType = app.AppType,
                AppActive = app.AppActive
            };
        }


        public ICollection<ApplicationViewModel> getAllApplications() {
            ICollection<ApplicationViewModel> result = new List<ApplicationViewModel>(); ;
            ICollection<Application> dbApps =  db.Applications.ToList();
            foreach (var app in dbApps) {
                result.Add(new ApplicationViewModel() {
                    AppID = app.AppID,
                    AppDateCreated = app.AppDateCreated,
                    AppDescription = app.AppDescription,
                    AppType = app.AppType,
                    AppActive = app.AppActive
                });

            }
            return result;
        }

        public void updateApp(int id, ApplicationViewModel application)
        {
            Application app = db.Applications.Find(id);
            app.AppDescription = application.AppDescription;
            app.AppType = application.AppType;
            app.AppActive = application.AppActive;
            db.Entry(app).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void createNewApp(ApplicationViewModel applicationViewModel)
        {
            Application app = new Application()
            {
                AppDescription = applicationViewModel.AppDescription,
                AppType = applicationViewModel.AppType,
                AppActive = true,
                AppDateCreated = DateTime.UtcNow.ToLocalTime()
            };
            db.Applications.Add(app);
            db.SaveChanges();
        }
    }
}
