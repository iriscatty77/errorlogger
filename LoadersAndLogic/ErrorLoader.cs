﻿using DBModel;
using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LoadersAndLogic
{
    public class ErrorLoader
    {
        private ErrorDBModel db = new ErrorDBModel();

        private static ApplicationViewModel app = null;

        public ICollection<ErrorViewModel> GetAllErrors() {

            ICollection<ErrorViewModel> result = new List<ErrorViewModel>();
            ApplicationViewModel appViewModel = null;
            db.Errors.ToList().ForEach(delegate (Error error)
            {
                if (appViewModel == null) {
                    app = new AppLoader().getApplication(error.AppID);

                }
                ErrorViewModel cError = new ErrorViewModel() {
                    ErrorID = error.ErrorID,
                    ErrorType = error.ErrorType,
                    ErrorDescription = error.ErrorDescription,
                    ErrorDateCreated = error.ErrorDateCreated,
                    AppID = error.AppID,
                    ApplicationViewModel = app
                };
                result.Add(cError);

            });
            return result;
        }

        public bool isUserAthorized(string user, int appID)
        {
            User dbUser = db.Users.Include(u => u.Applications)
                                .Include(u => u.Role)
                                .Where(u => u.EmailAddress == user)
                                .Single();
            if (dbUser.Role.RoleID == 8)
                return true;
            ICollection<int> appIDs = new HashSet<int>(dbUser.Applications.Select(i => i.AppID));
            return (appIDs.Contains(appID));
            
        }

        public ICollection<ErrorViewModel> GetErrorsByAppID(int appID) {

            ICollection<ErrorViewModel> result = new List<ErrorViewModel>();
            List<Error> appErrors = db.Errors
                                            .Where(i => i.AppID == appID)
                                            .ToList();
            ApplicationViewModel appViewModel = null;
            appErrors.ForEach(delegate (Error error)
            {
                if (appViewModel == null)
                {
                    app = new AppLoader().getApplication(error.AppID);

                }
                ErrorViewModel cError = new ErrorViewModel()
                {
                    ErrorID = error.ErrorID,
                    ErrorType = error.ErrorType,
                    ErrorDescription = error.ErrorDescription,
                    ErrorDateCreated = error.ErrorDateCreated,
                    AppID = error.AppID,
                    ApplicationViewModel = app
                };
                result.Add(cError);

            });
            return result;
        }


        public void SaveError(Common.ErrorViewModel error)
        {
            //DBModel.Application _app = new Application()
            //{
            //    AppID = error.AppID
            //    //AppDescription = "First App",
            //    //AppDateCreated = DateTime.UtcNow
            //};

            //Debug.WriteLine("================: set app id: " + error.AppID);
            Application _app = db.Applications.Find(error.AppID);
            Debug.WriteLine("================: db app id: " + _app.AppID);
            Error dbError = new Error() {
                ErrorDescription = error.ErrorDescription,
                ErrorDateCreated = error.ErrorDateCreated,
                // AppID = _app.AppID,
                Application = _app
            };
            

            // Debug.WriteLine("*********: " + dbError.Application.AppID + " **********");
            // Debug.WriteLine("*********: " + dbError.Application.AppDescription + " **********");

            // db.Applications.Attach(_app);
            db.Errors.Add(dbError);
            db.SaveChanges();

            Debug.WriteLine("********* " + " Saved !!!!!!!!!!!!!!!!!!!" + " **********");
        }


         
    }
}
