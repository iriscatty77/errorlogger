﻿namespace Common
{
    /// <summary>
    /// Enumeration for all possible roles that the user can have
    /// </summary>
    public enum RoleEnum
    {
        None = 0,

        User = 3,

        Admin = 8
    }
}
