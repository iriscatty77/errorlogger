﻿
namespace Common
{
    /// <summary>
    /// Contains controller and action constants
    /// </summary>
    public static class WebConstants
    {
        // Actions
        public static string HOME_ADMIN_PAGE = "AdminPage";

        public static string HOME_USER_PAGE = "UserPage";

        public static string ERROR_PAGE = "Error";

        public static string HOME_PAGE = "Index";

        // Controllers
        public static string HOME_CONTROLLER = "Home";
    }
}