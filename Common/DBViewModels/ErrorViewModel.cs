﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common
{
    public class ErrorViewModel
    {
        [Display(Name = "Error ID")]
        public int ErrorID { get; set; }
        [Display(Name = "Error Description")]
        public string ErrorDescription { get; set; }
        [Display(Name = "Error Type")]
        public int ErrorType { get; set; }
        [Display(Name = "Date Generated")]
        public DateTime? ErrorDateCreated { get; set; }
        [Display(Name = "App ID")]
        public int AppID { get; set; }
        public ApplicationViewModel ApplicationViewModel { get; set;}
    }
}
