﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ApplicationViewModel
    {
        [Display(Name = "App ID")]
        public int AppID { get; set; }
        [Display(Name = "Name")]
        public string AppDescription { get; set; }
        [Display(Name = "Type")]
        public int AppType { get; set; }
        [Display(Name = "Date Created")]
        public DateTime AppDateCreated { get; set; }
        // The status of this application, have column in DB
        [Display(Name = "Active Status")]
        public bool AppActive { get; set; }
        // The staus of the app for each user, no column in DB
        public bool Active { get; set; }
        public virtual ICollection<ErrorViewModel> ErrorViewModels { get; set; }
        public virtual ICollection<UserViewModel> UserViewModels { get; set; }
    }
}
