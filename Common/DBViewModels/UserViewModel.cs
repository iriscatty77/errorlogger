﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class UserViewModel
    {
        [Display(Name = "User ID")]
        public int UserID { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }
        [Display(Name = "Password")]
        public string UserPassword { get; set; }
        [Display(Name = "Last Login Date")]
        public DateTime? LastLoginDate { get; set; }
        [Display(Name = "Status ID")]
        public int? StateID { get; set; }
        [Display(Name = "Role ID")]
        public int? RoleID { get; set; }
        public virtual RoleViewModel RoleViewModel { get; set; }
        public virtual StateViewModel StateViewModel { get; set; }
        public ICollection<ApplicationViewModel> ApplicationViewModel { get; set; }

        public ICollection<string> StateDescs { get; set; }
        public ICollection<string> RoleDescs { get; set; }
    }
}
