﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class RoleViewModel
    {
        [Display(Name = "Role ID")]
        public int RoleID { get; set; }
        [Display(Name = "Role Desc")]
        public string RoleDescript { get; set; }
        public virtual ICollection<UserViewModel> Users { get; set; }
    }
}
