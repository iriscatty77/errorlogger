﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class StateViewModel
    {
        [Display(Name = "State ID")]
        public int StateID { get; set; }
        [Display(Name = "Status")]
        public string StateDescript { get; set; }
        public virtual ICollection<UserViewModel> Users { get; set; }
    }
}
