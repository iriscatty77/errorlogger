﻿/*

This is a dumb example to make the logger harness work.. This should be removed... 

*/

using System;
using System.Collections.Generic;
using System.Net.Http;
using Common;
using LoadersAndLogic;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;

namespace DLL
{

    public class Logger
    {
    
        private static int SERVICE_PORT = 50770;
        private static string SERVICE_URL = "http://localhost:{0}/";
        // private static string SERVICE_URL = "http://localhost/RestService/";

        private static string GET_ERRORS = "Api/Error";
        private static HttpClient client = new HttpClient();
        private int appID;

        private BlockingCollection<ErrorViewModel> errorQueue = new BlockingCollection<ErrorViewModel>();

        //private Queue<ErrorViewModel> errorQueue = new Queue<ErrorViewModel>();

        /// <summary>
        /// Constructor. Put whatever initialization code in here that you need
        /// </summary>
        public Logger(int appID)
        {
            this.appID = appID;
            client.BaseAddress = new Uri(String.Format(SERVICE_URL, SERVICE_PORT));

            //client.BaseAddress = new Uri(SERVICE_URL);

            //client.Timeout = TimeSpan.FromMinutes(60);
            StartSenderThread();
        }

        private void StartSenderThread()
        {
            new Thread(async () =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (true)
                {
                    if (errorQueue.Count == 0) {
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        ErrorViewModel er = errorQueue.Take();
                        Debug.WriteLine("======== Posting from queue ... =======");

                        try
                        {
                            HttpResponseMessage response = await client.PostAsJsonAsync(GET_ERRORS, er);
                            response.EnsureSuccessStatusCode();
                        }
                        catch (Exception e) {
                            Debug.WriteLine("Post failed again, add back to queue. " + e);
                            errorQueue.Add(er);
                        }
                       
                    }
                }

                //Console.WriteLine("Hello, world");
            }).Start();
        }

        /// <summary>
        /// This method is called by the test harness. So inside of it you should call your logger..
        /// </summary>
        /// <param name="errorMessage">Error Message</param>
        /// <param name="logLevel">Error Log Level</param>
        /// <param name="ex">Optional Exception</param>
        public async void Log(string errorMessage, int logLevel, Exception ex = null)
        {
            // this is a stub to allow us to do mean things....
            // Console.WriteLine(errorMessage);

            ErrorViewModel error = new ErrorViewModel()
            {
                ErrorDescription = errorMessage,
                ErrorDateCreated = DateTime.UtcNow.ToLocalTime(),
                AppID = appID
            };

            
            try {
                HttpResponseMessage response = await client.PostAsJsonAsync(GET_ERRORS, error);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception e) {
                Debug.WriteLine(e);
                errorQueue.Add(error);
                Debug.WriteLine("error added to queue.");
            }
            

        }


        // private void InitHttpClient()
        // {
        //client.BaseAddress = new Uri(String.Format(SERVICE_URL, SERVICE_PORT));
        //client.DefaultRequestHeaders.Accept.Clear();
        //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        // }

        //         private async void CreateErrorAsync(Error error)
        //         {
        //         HttpResponseMessage response = await client.PostAsJsonAsync(GET_ERRORS, error);
        //        response.EnsureSuccessStatusCode();
        //         }
    }
}
