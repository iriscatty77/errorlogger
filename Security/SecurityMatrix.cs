﻿/*
    Dusan Palider
    CSE 686
    Spring 2016

    Please consult global.asax for information about this code
*/

namespace Security
{
    using System.Collections.Generic;
    using Common;

    /// <summary>
    /// Security Matrix holds on to the minimal roles required for each controller/page combination
    /// </summary>
    public static class SecurityMatrix
    {
        /// <summary>
        /// Security Matrix, containing all of the controller/actions and the needed roles
        /// </summary>
        public static ICollection<SecurityAccess> Matrix { get; set; }

        /// <summary>
        /// Initializes the security matrix
        /// </summary>
        public static void Initialize()
        {
            Matrix = new List<SecurityAccess>()
            {
                #region Home Controller

                new SecurityAccess()
                {
                    Controller = WebConstants.HOME_CONTROLLER,
                    Action = WebConstants.HOME_PAGE,
                    MinimumRoleNeeded = RoleEnum.None
                },
                new SecurityAccess()
                {
                    Controller = "User",
                    Action = "Index",
                    MinimumRoleNeeded = RoleEnum.Admin
                },
                new SecurityAccess()
                {
                    Controller = "User",
                    Action = "Details",
                    MinimumRoleNeeded = RoleEnum.Admin
                },
                new SecurityAccess()
                {
                    Controller = "User",
                    Action = "Edit",
                    MinimumRoleNeeded = RoleEnum.Admin
                },
                new SecurityAccess()
                {
                    Controller = "User",
                    Action = "CurrentUser",
                    MinimumRoleNeeded = RoleEnum.User
                },
                
                new SecurityAccess()
                {
                    Controller = "User",
                    Action = "Index",
                    MinimumRoleNeeded = RoleEnum.Admin
                },

                new SecurityAccess()
                {
                    Controller = "Error",
                    Action = "AppErrors",
                    MinimumRoleNeeded = RoleEnum.User
                },
                new SecurityAccess()
                {
                    Controller = "Application",
                    Action = "Index",
                    MinimumRoleNeeded = RoleEnum.Admin
                },
                new SecurityAccess()
                {
                    Controller = "Application",
                    Action = "Details",
                    MinimumRoleNeeded = RoleEnum.Admin
                },
                      new SecurityAccess()
                {
                    Controller = "Application",
                    Action = "Edit",
                    MinimumRoleNeeded = RoleEnum.Admin
                },
                new SecurityAccess()
                {
                    Controller = "Application",
                    Action = "Create",
                    MinimumRoleNeeded = RoleEnum.Admin
                }



                #endregion
            };
        }
    }
}
