﻿/*
    Dusan Palider
    CSE 686
    Spring 2016

    Please consult global.asax for information about this code
*/

namespace Security
{
    using Common;

    /// <summary>
    /// Class used to store Security Matrix settings
    /// </summary>
    public class SecurityAccess
    {
        /// <summary>
        /// Action
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Controller
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Minimum needed role
        /// </summary>
        public RoleEnum MinimumRoleNeeded { get; set; }
    }
}
