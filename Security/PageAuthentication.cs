﻿/*
    Dusan Palider
    CSE 686
    Spring 2016

    Please consult global.asax for information about this code
*/

namespace Security
{
    using System.Linq;
    using Common;
    using LoadersAndLogic;
    using System.Web;
    using System.Diagnostics;
    using System;
    using System.Collections.Generic;

    public class PageAuthentication
    {
        RoleLoader roleLoader = new RoleLoader();

        public bool IsUserAuthorized(string controller, string action, string userName)
        {
            // userName is the email address
            RoleEnum userRole = roleLoader.getRoleByUserName(userName);
            Util.currentUserRoleEnum= userRole;

            Debug.WriteLine(userRole);
            Debug.WriteLine(controller);
            Debug.WriteLine(action);
            Debug.WriteLine(userName);

            // RoleEnum requiredRole = getRequiredRoleEnum(controller, action);
            // get required role from the Matrix (this will fail if we haven't registered the requested controller/action combination

            RoleEnum requiredRole = RoleEnum.None;
            try
            {
                requiredRole = SecurityMatrix.Matrix.First(x => x.Controller == controller && x.Action == action).MinimumRoleNeeded;

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return userRole >= requiredRole;

        }
    }
}
